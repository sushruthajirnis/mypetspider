import scrapy
from scrapy.http import Request
from scrapy.contrib.spiders import CrawlSpider,Rule
from scrapy.contrib.linkextractors import LinkExtractor
from newegg.items import NeweggItem
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
import re
class NeweggSpider(CrawlSpider):
    count=0
    name='newegg'
    allowed_domains=['newegg.com']
    start_urls=['http://www.newegg.com']
    rules=[Rule(LinkExtractor(allow=(ur'(\/Laptops\S[^\?]+\/*)'),),callback='parse_lap'),
            Rule(LinkExtractor(allow=(ur'(\/Cell\S[^\?]+\/*)'),),callback='parse_desktop')]
           # Rule(LinkExtractor.(allow=['/Desktop-PCs/Category/ID-228/*']),callback="parse_lap"),
           # Rule(LinkExtractor(allow=['/Chromebooks/Category/ID-269/*']),callback="parse_lap"),
           # Rule(LinkExtractor(allow=['/2-in-1-Ultrabooks/Category/ID-259/*']),callback="parse_lap"),
           # Rule(LinkExtractor(allow=['/MobileSubCategory/ID-3413/*']),callback="parse_lap"),
           # Rule(LinkExtractor(allow=['/Tablets/Category/ID-164/*']),callback="parse_lap"),
           # Rule(LinkExtractor(allow=['/All-Laptop-Accessories/Category/ID-26/*']),callback="parse_lap"),
           # Rule(LinkExtractor(allow=['/Keyboards-Mice/Category/ID-234/*']),callback="parse_lap") ]
    def get_product_type_from_url(self,url):
        return url.split("/")[3] if url.split("/")[3] else 'invalid_product_type'
    def parse_lap(self,response):
        print response.url
        self.count +=1
        item=NeweggItem()
        item['Product_category']='Computer Products'
        item['Product_type']=self.get_product_type_from_url(response.url)
        item['item_list']= response.xpath("//*[contains(@id,'titleDescriptionID')]//text()").extract()


        return item
             #item['model_url']=response.xpath(".//div[@class='foot']//li[@class='first']//a/@href").extract()[0]
        #item['Product_type']= response.url.split("/")[-3] if len(response.url.split("/"))>=3 else "insufficient data"
                #request.meta['item']=item

    def parse_desktop(self,response):
        print "Desktop urls as below********************"
        print response.url
        self.count+=1
        item=NeweggItem()
        item['Product_category']="Desktop Specialities"
        item['Product_type']=self.get_product_type_from_url(response.url)
        item['item_list']= response.xpath("//*[contains(@id,'titleDescriptionID')]//text()").extract()
        return item

    def parse_categories(self,response):
        print "In parse categories"
        print response.xpath(".//*[@id='blaNavigation']//dl/dd/a/@href").extract()[0]

    def parse_cat(self,response):
        print "In parse cat"
        item=response.request.meta['item']
        item['item_count']=response.xpath(".//div[@class='recordCount']//text()").extract()[3]
        print item['item_count']
        yield item
