# -*- coding: utf-8 -*-

# Scrapy settings for newegg project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'newegg'

SPIDER_MODULES = ['newegg.spiders']
NEWSPIDER_MODULE = 'newegg.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'newegg (+http://www.yourdomain.com)'

ITEM_PIPELINES={
        'newegg.pipelines.JsonWriterPipeline':'300'
        }
