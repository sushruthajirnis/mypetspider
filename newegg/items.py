# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class NeweggItem(scrapy.Item):
    Product_category=scrapy.Field()
    item_list=scrapy.Field()
    Product_type=scrapy.Field()
