# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import codecs
import json
from scrapy import signals
from collections import OrderedDict
from scrapy.contrib.exporter import JsonLinesItemExporter
class NeweggPipeline(object):
    def process_item(self, item, spider):
     return item

class JsonLineItemExporter(JsonLinesItemExporter):
    def __init__(self, file, **kwargs):
        self._configure(kwargs)
        self.file = file
        self.encoder = json.JSONEncoder(**kwargs)
        self.first_item = True
    def start_exporting(self):
        self.file.write("[")
    def finish_exporting(self):
        self.file.write("]")
    def export_item(self, item):
        if self.first_item:
            self.first_item = False
        else:
            self.file.write(',\n')
        itemdict = dict(self._get_serialized_fields(item))
        self.file.write(self.encoder.encode(itemdict))

class JsonWriterPipeline(object):
    def __init__(self):
        self.files = {}
    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def spider_opened(self,spider):
        file=codecs.open("data_counts_name.json",'w',encoding="utf-8")
        self.files[spider]=file
        self.exporter=JsonLineItemExporter(file)
        self.exporter.start_exporting()

    def process_item(self,item,spider):
        self.exporter.export_item(item)
        #line=json.dumps(OrderedDict(item),ensure_ascii=False,sort_keys=False) +"\n"
        #self.file.write(line)
        return item

    def spider_closed(self,spider):
        self.exporter.finish_exporting()
        file=self.files.pop(spider)
        file.close()
        #self.file.close()

